#!/usr/bin/env sh

AUTH_RESPONSE=`curl -sS -H 'Content-Type: application/x-www-form-urlencoded' \
      -XPOST $LOGIN_URL \
      -d 'grant_type=client_credentials&client_id='"$CLIENT_ID"'&client_secret='"$CLIENT_SECRET"`
ACCESS_TOKEN=$(echo $AUTH_RESPONSE | jq -r .access_token)
EXPORT_RESPONSE=`(time -f "elapsed %e" curl -i -sS -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer '"$ACCESS_TOKEN"'' \
  -XPOST "$EXPORT_URL" \
  -d '{"cronjob_mode": true, "table_id": "'"$TABLE_ID"'", "time_delay": '$TIME_DELAY'}') 2>&1`

statuscode=$(echo "$EXPORT_RESPONSE" | awk '/HTTP/ {print $2}')

if [ $statuscode -gt 199 -a $statuscode -lt 300 ]
then
  time=$(echo "$EXPORT_RESPONSE" | awk '/elapsed/ {print $2}')

  s="statusCode|elapsedTime
  $statuscode|$time"
  echo "$s" | jq -Rn '
  ( input  | split("|") ) as $keys |
  ( inputs | split("|") ) as $vals |
  [[$keys, $vals] | transpose[] | {key:.[0],value:.[1]|tonumber}] | from_entries
  ' | tr -d '\n'

else
  s="statusCode
  $statuscode"
  echo "$s" | jq -Rn '
  ( input  | split("|") ) as $keys |
  ( inputs | split("|") ) as $vals |
  [[$keys, $vals] | transpose[] | {key:.[0],value:.[1]|tonumber}] | from_entries
  ' | tr -d '\n'

fi
