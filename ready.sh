#!/usr/bin/env bash

echo "Script version 1.0"

LAST_IMAGE=$1
POD_NAME=$2
NAMESPACE=$3

for run in {1..50}
do
    declare -a Pods;
    Pods=($(kubectl get pods -n "$NAMESPACE" | grep "$POD_NAME" | awk '{ print $1 }'));
    echo "Validating deployed pods $run..."
    for Pod in "${Pods[@]}"
    do
        echo "Pod name: $Pod"
        CANDIDATE=($(kubectl describe pod "$Pod" -n "$NAMESPACE" | grep 'Image:\ \|Ready:\ ' | awk '{ print $2 }'))
        IS_LAST_IMAGE=false
        IS_READY=false
        for Line in "${CANDIDATE[@]}"
        do
            echo "Test data: $Line"
            if [ "$Line" = "$LAST_IMAGE" ]
            then
                IS_LAST_IMAGE=true;
            fi
            if [ "$Line" = "True" ]
            then
                IS_READY=true;
            fi
        done

        if [ "$IS_LAST_IMAGE" = true ] && [ "$IS_READY" = true ]
        then
            echo "Pod deployed successfully $LAST_IMAGE"
            exit 0;
        fi
    done;
    sleep 10;
done;

echo "There is no pod using the given Image and in ready state" >&2
exit 1;
